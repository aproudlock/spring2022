<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Loops</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css" />
</head>
<body>
<header><?php include('../Template/header.php'); ?></header>
<nav><?php include('../Template/nav.php'); ?> </nav>

<main>
    <h1>Loops and String Functions Demo</h1>

    <h3>Basic Vars and Stuff</h3>
    <?php
        $number = 100;
        print $number;

        echo "<br><strong>".$number."</strong>";

        echo "<br><strong>";
        echo $number;
        echo "</strong>";

        echo "<br><strong>$number</strong>";
        echo '<br><strong>$number</strong>';
        echo "<br><strong>{$number}</strong>";

        $result = "<br><strong>";
        $result .= $number;
        $result .= "</strong>";
        echo $result;
    ?>
    <h3>Math</h3>
    <?php
        $number_1 = 100;
        $number_2 = "50";
        $number_3 = 50;

        $result_1 = $number_1 + $number_2;
        $result_2 = $number_1 + $number_3;

        echo "<br>".$result_1;
        echo "<br>".$result_2;
    ?>
    <h3>While Loop</h3>
    <?php
        $i = 1;
        while($i < 7) {
            echo "<h$i>Howdy World!</h$i>";
            //$i = $i + 1;
            //$i += 1;
            $i++;
          }
    ?>

    <h3>Do While Loop</h3>
    <?php
        $i = 6;
        do{
            echo "<h$i>Howdy World!</h$i>";
            //$i = $i - $i;
            //$i -= 1;
            $i--;
        }while($i > 0);
    ?>

    <h3> For Loop </h3>
    <?php
        for($i = 1; $i <= 6; $i++){
            echo "<h$i>Howdy World!</h$i>";
        }
    ?>

    <h3>More String Fun</h3>
    <?php
        $full_name = 'Bob Smith';
        //$full_name = 123;
        //$full_name = false;

        // b o b   s m i t h
        // 0 1 2 3 4 5 6 7 8
        $position = strpos($full_name, " ");
        echo "<br> The space position is in position $position";

        echo "".$full_name;
        echo "<br>".strtoupper($full_name);
        echo "<br>".strtolower($full_name);

        $name_parts = explode(" ", $full_name);
        echo "<br>First Name:".$name_parts[0];
        echo "<br>Last Name:".$name_parts[1];



    ?>




</main>
<footer><?php include('../Template/footer.php'); ?></footer>
</body>
</html>