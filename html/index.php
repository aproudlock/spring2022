<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title> Abbie's Homepage</title>
        <link rel="stylesheet" type="text/css" href="css/base.css" />
    </head>
    <body>
        <header><?php include('Template/header.php'); ?></header>
        <nav><?php include('Template/nav.php'); ?> </nav>
        <main>
            <img src="img/abbie.JPG" alt="Abbie's Image" />
            <p>Praesent suscipit augue sed nisi suscipit ullamcorper. Suspendisse sodales lacus et eros sodales, at pellentesque magna pharetra. Praesent iaculis viverra metus, sit amet porta dolor dictum aliquam. Mauris nec lectus facilisis, convallis eros eu, accumsan dui. Integer convallis quis velit id tempus. Maecenas laoreet sed eros vel luctus. Cras nibh nibh, volutpat in sem eget, mattis efficitur urna. Aenean eleifend ut dui sed vestibulum. Nunc diam lacus, sagittis sed semper ac, condimentum scelerisque dolor.</p>
        </main>
        <footer><?php include('Template/footer.php'); ?></footer>
    </body>
</html>